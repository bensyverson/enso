Ensō, a macOS Dvorak keyboard layout, providing:

- Macron dead key (`Option-Shift ,`)
- Common English words (`Control-Option-Shift`)
- Common Swift keywords (`Control-Option`)